var fs = require('fs');
var mongojs = require('mongojs');
var db = mongojs('logs', ['apilogs']);


var day = new Date().getDate();
var month = new Date().getMonth() + 1;
var year = new Date().getYear() + 1900;
var outputfile = "./log-" + day + "-" + month + "-" + year + ".tsv";
var outstream = fs.createWriteStream(outputfile, {
  'flags': 'a'
});

outstream.writable = true;

var log = function(inputObj) {
  return new Promise(function(resolve, reject) {
    if (inputObj && typeof inputObj == "object" && inputObj.length > 0) {
      for (var i = 0; i < inputObj.length; i++) {
        item = inputObj[i];
        if (undefined == item.api || item.api == '')
          item.api = 'Unknown';
        else
          item.api.replace('\t', ' ');
        // item.ts = new Date();
        if (undefined == item.ts || item.ts == '') {
          console.log("Invalid timeStamp");
          break;
        } else
          item.ts.replace('\t', ' ');
        if (undefined == item.txn || item.txn == '') {
          console.log("Invalid txn");
          break;
        } else
          item.txn.replace('\t', ' ');
        if (undefined == item.info || item.info == '')
          item.info = 'INFO';
        else
          item.info.replace('\t', ' ');
        if (undefined == item.msg)
          item.msg = '';
        else
          item.msg.replace('\t', ' ');
        if (undefined == item.data)
          item.data = '';
        else if (typeof item.data != 'string')
          item.data = JSON.stringify(item.data);
        // outstream.write(item.api + "\t" + item.ts + "\t" + item.txn + "\t" + item.info + "\t" + item.msg + "\t" + item.data + '\n');
        db.apilogs.insert({
          api: item.api,
          timeStamp: item.ts,
          txn: item.txn,
          info: item.info,
          msg: item.msg,
          data: item.data
        });
      }
      resolve(true);
    } else
      reject(false);
  })
}

module.exports = {};
module.exports.log = log;

//
// var obj1 = [{
//   api: "api",
//   txn: "gatewaytxn/apitxn/organstxn/helpertxn",
//   ts: "timestamp",
//   info: "INFO/WARN/ERROR",
//   msg: "Task Allowed",
//   data: {
//     data: "Some stringified data"
//   }
// }]
//
// log(obj1)
//   .then(function(response) {
//     console.log(response);
//     return;
//   })
//   .catch(function(error) {
//     console.log(error);
//     return;
//   });

//Sample inputObj
//{
// api: "api" //snake case
// txn: "gatewaytxn/apitxn/organstxn/helpertxn"
// info: "INFO/WARN/ERROR"
// msg: "Task Allowed"
// data: "Some stringified data"
// }
