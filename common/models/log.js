'use strict';
var logger = require('../../modules/logAll.js');

module.exports = function(Log) {

  Log.disableRemoteMethod("find", true);
  Log.disableRemoteMethod("findById", true);
  Log.disableRemoteMethod("update", true);
  Log.disableRemoteMethod("exists", true);
  Log.disableRemoteMethod("upsert", true);
  Log.disableRemoteMethod("count", true);
  Log.disableRemoteMethod("delete", true);
  Log.disableRemoteMethod("deleteById", true);
  Log.disableRemoteMethod("updateAll", true);
  Log.disableRemoteMethod("createChangeStream", true);
  Log.disableRemoteMethod("findOne", true);
  Log.disableRemoteMethod("updateAttributes", false);

  Log.afterRemote('create', function(ctx, result, next) {

    if (result == [] || result.logArray == []) {
      var toSendError = new Error("No logs to log");
      toSendError.statusCode = 400;
      next(toSendError)
    } else {
      logger.log(result.logArray)
        .then(function(response) {
          console.log("Response: ", response);
          result.result = response;
          next()
        })
        .catch(function(err) {
          console.log("Error: ", err);
          var toSendError = new Error("Not all logs were logged for some reason");
          toSendError.statusCode = 400;
          next(toSendError)
        });
    }
  });
};
